document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("formulario").addEventListener('submit', validarFormulario); 
  });
  
  function validarFormulario(evento) {

    evento.preventDefault();
    var usuario = document.getElementById('nombreReg').value;
    if(usuario.length == 0) {
      alert('No has escrito nada en el nombre');
      return;
    }

    var usuario = document.getElementById('apellidoReg').value;
    if(usuario.length == 0) {
      alert('No has escrito nada en el apellido');
      return;
    }

    var fecha = document.getElementById('fecha').value;
    if(fecha == '') {
      alert('Por favor ingresa una fecha');
      return;
    }

    var clave = document.getElementById('contraseña').value;
    var clave2 = document.getElementById('contraseña2').value;
    if (clave != clave2) {
      alert('Las claves no son iguales');
      return;
    }

    if (clave.length < 8) {
        alert('la clave debe tener mas de 8 caracteres');
        return;
      }

    if (clave == '' && clave2 == '') {
        alert('Ingrese contraseña');
        return;
      }

    this.submit();
  }

  $(function(){
    var genero = document.getElementById('fecha').value;
    console.log(genero)
  })
